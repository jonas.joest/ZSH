- install ZSH
- install oh my zsh
- install fonts
- set color scheme and font in terminal emulator
- font used: DejaVu Sans Moto Powerline Book

fonts to install:
- Powerline Fonts `sudo apt-get install fonts-powerline`
- (evtl. deja vu [...] from PowerLine fonts repository)
- (awesome terminal fonts: https://github.com/gabrielelana/awesome-terminal-fonts/tree/master/build)

You might be fine if you just install my merged terminal font in the `terminal font` folder
